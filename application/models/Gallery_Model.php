<?php 

class Gallery_Model extends CI_Model{
    // kategori
    public function allGallery()
    {
        return $this->db->get('galleries')->result_object();
	}
    public function getGallery($id)
    {
        return $this->db->get_where('galleries', ['id' => $id])->row();
    }
    public function addGallery($data)
    {
        return $this->db->insert('galleries', $data);
    }
    public function updateGallery($id, $data)
    {
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update('galleries');
    }
    public function deleteGallery($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('galleries');
    }
    public function searchGallery($keyword)
    {
        $this->db->like('title', $keyword);
        return $this->db->get('galleries')->result_object();
    }
    public function countGallery()
    {
        return $this->db->count_all('galleries'); 
    }
}