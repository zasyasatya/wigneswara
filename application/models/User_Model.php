<?php 

class User_Model extends CI_Model{
	public function getUser($username)
    {
        return $this->db->get_where('users', ['username' => $username])->row();
	}
    public function updateUser($id, $update)
    {
		return $this->db->where(["id" => $id])->update('users', $update);
    }
}