<?php 

class Activity_Model extends CI_Model{
    // kategori
    public function allCategory()
    {
        return $this->db->get('categories')->result_object();
	}
    public function getCategory($id)
    {
        return $this->db->get_where('categories', ['id' => $id])->row();
    }
    public function addCategory($data)
    {
        return $this->db->insert('categories', $data);
    }
    public function updateCategory($id, $data)
    {
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update('categories');
    }
    public function deleteCategory($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('categories');
    }
    // kegiatan
    public function allActivity()
    {
        $this->db->select('activities.id as id,  title, content, DATE_FORMAT(date, "%M %d, %Y") as date, categories.name as category, categories.description as description, categories.id as category_id, picture1, picture2, video');
        $this->db->from('activities');
        $this->db->join('users', 'users.id = activities.user');
        $this->db->join('categories', 'categories.id = activities.category');
        return $this->db->get()->result_object();
	}
    public function getActivity($id)
    {
        $this->db->select('activities.id as id, users.name as user, title, content, DATE_FORMAT(date, "%M %d, %Y") as date, categories.name as category, picture1, picture2, video');
        $this->db->from('activities');
        $this->db->join('users', 'users.id = activities.user');
        $this->db->join('categories', 'categories.id = activities.category');
        $this->db->where('activities.id', $id);
        return $this->db->get()->row();
    }
    public function addActivity($data)
    {
        return $this->db->insert('activities', $data);
    }
    public function updateActivity($id, $data)
    {
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update('activities');
    }
    public function deleteActivity($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('activities');
    }
    public function searchActivity($keyword)
    {
        $this->db->select('activities.id as id, users.name as user, title, content, date, categories.name as category, picture1, picture2, video');
        $this->db->from('activities');
        $this->db->like('title', $keyword);
        $this->db->or_like('categories.name', $keyword);
        $this->db->join('categories', 'categories.id = activities.category');
        $this->db->join('users', 'users.id = activities.user');
        return $this->db->get()->result_object();
    }
    public function countActivity()
    {
        return $this->db->count_all('activities'); 
    }
}