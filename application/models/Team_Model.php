<?php 

class Team_Model extends CI_Model{
    // divisi
	public function allDivision()
    {
        return $this->db->get('divisions')->result_object();
	}
    public function getDivision($id)
    {
        return $this->db->get_where('divisions', ['id' => $id])->row();
    }
    public function addDivision($data)
    {
        return $this->db->insert('divisions', $data);
    }
    public function updateDivision($id, $data)
    {
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update('divisions');
    }
    public function deleteDivision($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('divisions');
    }
    // position
    public function allPosition()
    {
        return $this->db->get('positions')->result_object();
    }
    // team
	public function allTeam()
    {
        $this->db->select('teams.id as id, nim, teams.name as name, prodi, divisions.name as division, positions.name as position');
        $this->db->from('teams');
        $this->db->join('divisions', 'divisions.id = teams.division');
        $this->db->join('positions', 'positions.id = teams.position');
        $this->db->order_by('division', 'ASC');
        return $this->db->get()->result_object();
	}
    public function getTeam($id)
    {
        return $this->db->get_where('teams', ['id' => $id])->row();
	}
    public function addTeam($data)
    {
        return $this->db->insert('teams', $data);
    }
    public function updateTeam($id, $data)
    {
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update('teams');
    }
    public function deleteTeam($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('teams');
    }
    public function countTeam()
    {
        return $this->db->count_all('teams'); 
    }
}