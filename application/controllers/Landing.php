<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Landing extends CI_Controller {
    public function __construct() 
    {
		parent::__construct();
		$this->load->model('Activity_Model');
		$this->load->model('Gallery_Model');
		$this->load->model('Team_Model');
		$this->load->library('form_validation');
        $this->session->set_userdata('referred_from', current_url());
	}
    /*
        *
        *
        BERANDA
        *
        *
    */
	public function index()
	{
        $data['header'] = 'Undiksha';
        $data['galleries'] = $this->Gallery_Model->allGallery();
        $this->load->view('landing/master/header', $data);
        $this->load->view('landing/master/nav_home');
        $this->load->view('landing/index', $data);
		$this->load->view('landing/master/footer');
	}
    /*
        *
        *
        KEGIATAN
        *
        *
    */
    public function activities()
    {
        $data['header'] = 'Kegiatan';
        $data['nav'] = 'activities';
        $data['categories'] =  $this->Activity_Model->allCategory();
        $data['activities'] =  $this->Activity_Model->allActivity();
        $this->load->view('landing/master/header', $data);
        $this->load->view('landing/master/nav', $data);
        $this->load->view('landing/activities', $data);
		$this->load->view('landing/master/footer');
    }
    public function activity($id)
    {
        $data['activity'] =  $this->Activity_Model->getActivity($id);
        $data['header'] = 'Kegiatan : ' .$data['activity']->title;
        $data['nav'] = 'activities';
        $this->load->view('landing/master/header', $data);
        $this->load->view('landing/master/nav', $data);
        $this->load->view('landing/activity', $data);
		$this->load->view('landing/master/footer');
    }
    /*
        *
        *
        TENTANG
        *
        *
    */
    public function about()
    {
        $data['header'] = 'Tentang';
        $data['nav'] = 'about';
        $this->load->view('landing/master/header', $data);
        $this->load->view('landing/master/nav', $data);
        $this->load->view('landing/about');
		$this->load->view('landing/master/footer');
    }
    /*
        *
        *
        TIM
        *
        *
    */
    public function teams()
    {
        $data['header'] = 'Tim';
        $data['nav'] = 'teams';
        $data['teams'] = $this->Team_Model->allTeam();
        $this->load->view('landing/master/header', $data);
        $this->load->view('landing/master/nav', $data);
        $this->load->view('landing/teams', $data);
		$this->load->view('landing/master/footer');
    }
}