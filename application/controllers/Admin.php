<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
    public function __construct() 
    {
		parent::__construct();
		$this->load->model('User_Model');
		$this->load->model('Team_Model');
		$this->load->model('Activity_Model');
		$this->load->model('Gallery_Model');
		$this->load->library('form_validation');
        $this->session->set_userdata('referred_from', current_url());
	}
    /*
        *
        *
        BERANDA
        *
        *
    */
	public function index()
	{
        if($this->session->id == null){
            redirect('auth');
            die();
        }
        $data['nav'] = 'home';
        $data['activities'] = $this->Activity_Model->countActivity();
        $data['teams'] = $this->Team_Model->countTeam();
        $data['galleries'] = $this->Gallery_Model->countGallery();
        $this->load->view('admin/master/header', $data);
        $this->load->view('admin/index', $data);
		$this->load->view('admin/master/footer');
	}
    /*
        *
        *
        PENGATURAN
        *
        *
    */
	public function show_setting()
	{
        if($this->session->id == null){
            redirect('auth');
            die();
        }
        $data['nav'] = 'setting';
        $this->load->view('admin/master/header', $data);
        $this->load->view('admin/setting');
		$this->load->view('admin/master/footer');
	}
    public function update_user()
    {   
        if($this->session->id == null){
            redirect('auth');
            die();
        }
		$this->form_validation->set_rules('name', 'Name', 'required|trim');
        $this->form_validation->set_rules('username', 'Username', 'required|trim');
        
        if($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('error_update', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Nama dan username tidak boleh kosong!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button></div>');
        }else{
            $name = $this->input->post('name');
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $confirm = $this->input->post('confirm');
            if($password != ''){
                if($password == $confirm){
                    $update = [
                        "username" => htmlspecialchars($username),
                        "name" => htmlspecialchars($name),
                        "password" => password_hash($password, PASSWORD_DEFAULT),
                    ];
                }else{
                    $this->session->set_flashdata('error_update', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Password dan konfirmasi password tidak sama!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button></div>');
                }
            }else{
                $update = [
                    "username" => htmlspecialchars($username),
                    "name" => htmlspecialchars($name),
                ];
            }
        }
        if($this->User_Model->updateUser($this->session->id, $update)){
            $user= [
                'username' => $username,
                'name' => $name
            ];
            $this->session->set_userdata($user);
            $this->session->set_flashdata('error_update', '<div class="alert alert-success alert-dismissible fade show" role="alert">Data berhasil diperbaharui<button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button></div>');
        }else{
            $this->session->set_flashdata('error_update', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Terjadi kesalahan pada sistem!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button></div>');
        }
        redirect('admin/show_setting');
    }
    /*
        *
        *
        TENTANG
        *
        *
    */
	public function show_about()
	{
        if($this->session->id == null){
            redirect('auth');
            die();
        }
        $data['nav'] = 'about';
        $this->load->view('admin/master/header', $data);
        $this->load->view('admin/about');
		$this->load->view('admin/master/footer');
	}
    /*
        *
        *
        KEGIATAN
        *
        *
    */
	public function show_activity()
	{
        if($this->session->id == null){
            redirect('auth');
            die();
        }
        $search = $this->input->get('search');
        if(isset($search)){
            $data['activities'] = $this->Activity_Model->searchActivity($search);
        }else{
            $data['activities'] = $this->Activity_Model->allActivity();
        }
        $data['nav'] = 'activity';
        $this->load->view('admin/master/header', $data);
        $this->load->view('admin/activity/index');
		$this->load->view('admin/master/footer');
	}
	public function add_activity()
	{
        if($this->session->id == null){
            redirect('auth');
            die();
        }
        if($this->input->post()){
            $this->form_validation->set_rules('title', 'title', 'required|trim');
		    $this->form_validation->set_rules('category', 'category', 'required|trim');
		    $this->form_validation->set_rules('content', 'content', 'required|trim');
		    $this->form_validation->set_rules('video', 'video', 'required|trim');

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error_activity', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Semua kolom tidak boleh kosong!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button></div>');
                redirect('admin/add_activity');
            }else{
                $title = $this->input->post('title');
                $category = $this->input->post('category');
                $content = $this->input->post('content');
                $video = $this->input->post('video');

                $upload = $this->upload_files('assets/img/upload/activities/', ['picture1', 'picture2']);
                if($upload){
                    date_default_timezone_set("Asia/Singapore");
                    $insert = [
                        'user' => $this->session->id,
                        'title' => htmlspecialchars($title),
                        'category' => htmlspecialchars($category),
                        'content' => $content,
                        'date' => date('Y/m/d H:i:s'),
                        'category' => $category,
                        'picture1' =>  $upload[0],
                        'picture2' => $upload[1],
                        'video' => $video

                    ];
    
                    if($this->Activity_Model->addActivity($insert)){
                        $this->session->set_flashdata('error_activity', '<div class="alert alert-success alert-dismissible fade show mx-2 mt-2" role="alert">Kegiatan berhasil ditambahkan!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button></div>');
                        redirect('admin/show_activity');
                    }else{
                        $this->session->set_flashdata('error_activity', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Terjadi kesalahan pada sistem!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button></div>');
                        redirect('admin/add_activity');
                    }

                }else{
                    $this->session->set_flashdata('error_activity', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Gagal saat upload gambar. Pastikan gambar berformat jpeg, jpg, atau png dan kurang dari 3Mb!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button></div>');
                    redirect('admin/add_activity');
                }
            }
        }else{
            $data['nav'] = 'activity';
            $data['header'] = 'Tambah';
            $data['categories'] = $this->Activity_Model->allCategory();
            $this->load->view('admin/master/header', $data);
            $this->load->view('admin/activity/add_activity');
            $this->load->view('admin/master/footer');
        }
	}
    public function edit_activity($id)
    {
        if($this->session->id == null){
            redirect('auth');
            die();
        }
        if($this->input->post()){
            $this->form_validation->set_rules('title', 'title', 'required|trim');
		    $this->form_validation->set_rules('category', 'category', 'required|trim');
		    $this->form_validation->set_rules('content', 'content', 'required|trim');

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error_activity', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Semua kolom tidak boleh kosong!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button></div>');
                redirect('admin/edit_activity');
            }else{
                $title = $this->input->post('title');
                $category = $this->input->post('category');
                $content = $this->input->post('content');

                $update = [
                    'title' => htmlspecialchars($title),
                    'category' => htmlspecialchars($category),
                    'content' => $content,
                ];

                if($this->Activity_Model->updateActivity($id, $update)){
                    $this->session->set_flashdata('error_activity', '<div class="alert alert-success alert-dismissible fade show mx-2 mt-2" role="alert">Kegiatan berhasil diperbaharui!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button></div>');
                    redirect('admin/show_activity');
                }else{
                    $this->session->set_flashdata('error_activity', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Terjadi kesalahan pada sistem!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button></div>');
                    redirect('admin/edit_activity');
                }
            }
        }else{
            $data['nav'] = 'activity';
            $data['header'] = 'Edit';
            $data['categories'] = $this->Activity_Model->allCategory();
            $data['activity'] = $this->Activity_Model->getActivity($id);
            $this->load->view('admin/master/header', $data);
            $this->load->view('admin/activity/add_activity');
            $this->load->view('admin/master/footer');
        }
    }
    public function delete_activity($id)
    {
        if($this->session->id == null){
            redirect('auth');
            die();
        }
        $activity = $this->Activity_Model->getActivity($id);
        $path = 'assets/img/upload/activities/';
        if(unlink($path.$activity->picture1) && unlink($path.$activity->picture2)){
            $this->Activity_Model->deleteActivity($id);
            $this->session->set_flashdata('error_activity', '<div class="alert alert-success alert-dismissible fade show mx-2 mt-2" role="alert">Kegiatan berhasil dihapus!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            redirect('admin/show_activity');
        }else{
            $this->session->set_flashdata('error_activity', '<div class="alert alert-danger alert-dismissible fade show mx-2 mt-2" role="alert">Terjadi kesalahan saat menghapus gambar!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            redirect('admin/show_activity');
        }
        
    }
	public function show_category()
	{
        if($this->session->id == null){
            redirect('auth');
            die();
        }
        $data['nav'] = 'activity';
        $data['categories'] = $this->Activity_Model->allCategory();
        $this->load->view('admin/master/header', $data);
        $this->load->view('admin/activity/category', $data);
		$this->load->view('admin/master/footer');
	}
	public function add_category()
	{
        if($this->session->id == null){
            redirect('auth');
            die();
        }

        if($this->input->post()){
            $this->form_validation->set_rules('name', 'name', 'required|trim');
		    $this->form_validation->set_rules('description', 'description', 'required|trim');

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error_category', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Nama kategori dan deskripsi tidak boleh kosong!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button></div>');
                redirect('admin/add_category');
            }else{
                $name = $this->input->post('name');
                $description = $this->input->post('description');
                $insert = [
                    'name' => htmlspecialchars($name),
                    'description' => htmlspecialchars($description)
                ];

                if($this->Activity_Model->addCategory($insert)){
                    $this->session->set_flashdata('error_category', '<div class="alert alert-success alert-dismissible fade show mx-2 mt-2" role="alert">Kategori berhasil ditambahkan!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button></div>');
                    redirect('admin/show_category');
                }else{
                    $this->session->set_flashdata('error_category', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Terjadi kesalahan pada sistem!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button></div>');
                    redirect('admin/add_category');
                }
            }
        }else{
            $data['nav'] = 'activity';
            $data['header'] = 'Tambah';
            $this->load->view('admin/master/header', $data);
            $this->load->view('admin/activity/add_category', $data);
		    $this->load->view('admin/master/footer');
        }
	}
	public function edit_category($id)
	{
        if($this->session->id == null){
            redirect('auth');
            die();
        }

        if($this->input->post()){
            $this->form_validation->set_rules('name', 'name', 'required|trim');
		    $this->form_validation->set_rules('description', 'description', 'required|trim');

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error_category', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Nama kategori dan deskripsi tidak boleh kosong!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button></div>');
                redirect('admin/add_category');
            }else{
                $name = $this->input->post('name');
                $description = $this->input->post('description');
                $update = [
                    'name' => htmlspecialchars($name),
                    'description' => htmlspecialchars($description)
                ];

                if($this->Activity_Model->updateCategory($id, $update)){
                    $this->session->set_flashdata('error_category', '<div class="alert alert-success alert-dismissible fade show mx-2 mt-2" role="alert">Kategori berhasil diperbaharui!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button></div>');
                    redirect('admin/show_category');
                }else{
                    $this->session->set_flashdata('error_category', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Terjadi kesalahan pada sistem!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button></div>');
                    redirect('admin/add_category');
                }
            }
        }else{
            $data['nav'] = 'activity';
            $data['header'] = 'Tambah';
            $data['category'] = $this->Activity_Model->getCategory($id);
            $this->load->view('admin/master/header', $data);
            $this->load->view('admin/activity/add_category', $data);
		    $this->load->view('admin/master/footer');
        }
	}
    public function delete_category($id)
    {
        if($this->session->id == null){
            redirect('auth');
            die();
        }

        $this->Activity_Model->deleteCategory($id);
        $this->session->set_flashdata('error_category', '<div class="alert alert-success alert-dismissible fade show mx-2 mt-2" role="alert">Kategori berhasil dihapus!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
        redirect('admin/show_category');
    }
    /*
        *
        *
        TIM
        *
        *
    */
    public function show_team()
	{
        if($this->session->id == null){
            redirect('auth');
            die();
        }

        $data['nav'] = 'team';
        $data['teams'] = $this->Team_Model->allTeam();
        $this->load->view('admin/master/header', $data);
        $this->load->view('admin/team/index', $data);
		$this->load->view('admin/master/footer');
	}
	public function add_team()
	{
        if($this->session->id == null){
            redirect('auth');
            die();
        }

        if($this->input->post()){
            $this->form_validation->set_rules('nim', 'nim', 'required|trim');
		    $this->form_validation->set_rules('name', 'name', 'required|trim');
		    $this->form_validation->set_rules('prodi', 'prodi', 'required|trim');
		    $this->form_validation->set_rules('division', 'division', 'required|trim');
		    $this->form_validation->set_rules('position', 'position', 'required|trim');

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error_team', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Semua kolom tidak boleh kosong!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button></div>');
                redirect('admin/add_team');
            }else{
                $nim = $this->input->post('nim');
                $name = $this->input->post('name');
                $prodi = $this->input->post('prodi');
                $division = $this->input->post('division');
                $position = $this->input->post('position');

                $insert = [
                    'nim' => $nim,
                    'name' => htmlspecialchars($name),
                    'prodi' => htmlspecialchars($prodi),
                    'division' => $division,
                    'position' => $position
                ];

                if($this->Team_Model->addTeam($insert)){
                    $this->session->set_flashdata('error_team', '<div class="alert alert-success alert-dismissible fade show mx-2 mt-2" role="alert">Anggota baru berhasil ditambahkan!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button></div>');
                    redirect('admin/show_team');
                }else{
                    $this->session->set_flashdata('error_team', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Terjadi kesalahan pada sistem!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button></div>');
                    redirect('admin/add_team');
                }
            }
        }else{
            $data['nav'] = 'team';
            $data['header'] = 'Tambah';
            $data['divisions'] = $this->Team_Model->allDivision();
            $data['positions'] = $this->Team_Model->allPosition();
            $this->load->view('admin/master/header', $data);
            $this->load->view('admin/team/add_team');
            $this->load->view('admin/master/footer');
        }
	}
    public function edit_team($id)
	{
        if($this->session->id == null){
            redirect('auth');
            die();
        }

        if($this->input->post()){
            $this->form_validation->set_rules('nim', 'nim', 'required|trim');
		    $this->form_validation->set_rules('name', 'name', 'required|trim');
		    $this->form_validation->set_rules('prodi', 'prodi', 'required|trim');
		    $this->form_validation->set_rules('division', 'division', 'required|trim');
		    $this->form_validation->set_rules('position', 'position', 'required|trim');

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error_team', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Semua kolom tidak boleh kosong!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button></div>');
                redirect('admin/edit_team');
            }else{
                $nim = $this->input->post('nim');
                $name = $this->input->post('name');
                $prodi = $this->input->post('prodi');
                $division = $this->input->post('division');
                $position = $this->input->post('position');

                $update = [
                    'nim' => $nim,
                    'name' => htmlspecialchars($name),
                    'prodi' => htmlspecialchars($prodi),
                    'division' => $division,
                    'position' => $position
                ];

                if($this->Team_Model->updateTeam($id, $update)){
                    $this->session->set_flashdata('error_team', '<div class="alert alert-success alert-dismissible fade show mx-2 mt-2" role="alert">Anggota berhasil diperbaharui!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button></div>');
                    redirect('admin/show_team');
                }else{
                    $this->session->set_flashdata('error_team', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Terjadi kesalahan pada sistem!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button></div>');
                    redirect('admin/edit_team');
                }
            }
        }else{
            $data['nav'] = 'team';
            $data['header'] = 'Edit';
            $data['team'] =$this->Team_Model->getTeam($id);
            $data['divisions'] = $this->Team_Model->allDivision();
            $data['positions'] = $this->Team_Model->allPosition();
            $this->load->view('admin/master/header', $data);
            $this->load->view('admin/team/add_team');
            $this->load->view('admin/master/footer');
        }
	}
    public function delete_team($id)
    {
        if($this->session->id == null){
            redirect('auth');
            die();
        }

        $this->Team_Model->deleteTeam($id);
        $this->session->set_flashdata('error_team', '<div class="alert alert-success alert-dismissible fade show mx-2 mt-2" role="alert">Anggota berhasil dihapus!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
        redirect('admin/show_team');
    }
	public function show_division()
	{   
        if($this->session->id == null){
            redirect('auth');
            die();
        }

        $data['divisions'] = $this->Team_Model->allDivision();
        $data['nav'] = 'team';
        $this->load->view('admin/master/header', $data);
        $this->load->view('admin/team/division');
		$this->load->view('admin/master/footer');
	}
	public function add_division()
	{
        if($this->session->id == null){
            redirect('auth');
            die();
        }

        if($this->input->post()){
            $this->form_validation->set_rules('name', 'name', 'required|trim');
		    $this->form_validation->set_rules('description', 'description', 'required|trim');

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error_division', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Nama divisi dan deskripsi tidak boleh kosong!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button></div>');
                redirect('admin/add_division');
            }else{
                $name = $this->input->post('name');
                $description = $this->input->post('description');
                $insert = [
                    'name' => htmlspecialchars($name),
                    'description' => htmlspecialchars($description)
                ];

                if($this->Team_Model->addDivision($insert)){
                    $this->session->set_flashdata('error_division', '<div class="alert alert-success alert-dismissible fade show mx-2 mt-2" role="alert">Divisi berhasil ditambahkan!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button></div>');
                    redirect('admin/show_division');
                }else{
                    $this->session->set_flashdata('error_division', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Terjadi kesalahan pada sistem!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button></div>');
                    redirect('admin/add_division');
                }
            }
        }else{
            $data['nav'] = 'team';
            $data['header'] = 'Tambah';
            $this->load->view('admin/master/header', $data);
            $this->load->view('admin/team/add_division', $data);
            $this->load->view('admin/master/footer');
        }
	}
    public function edit_division($id)
	{
        if($this->session->id == null){
            redirect('auth');
            die();
        }

        if($this->input->post()){
            $this->form_validation->set_rules('name', 'name', 'required|trim');
		    $this->form_validation->set_rules('description', 'description', 'required|trim');

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error_division', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Nama divisi dan deskripsi tidak boleh kosong!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button></div>');
                redirect('admin/edit_division');
            }else{
                $name = $this->input->post('name');
                $description = $this->input->post('description');
                $update = [
                    'name' => htmlspecialchars($name),
                    'description' => htmlspecialchars($description)
                ];

                if($this->Team_Model->updateDivision($id, $update)){
                    $this->session->set_flashdata('error_division', '<div class="alert alert-success alert-dismissible fade show mx-2 mt-2" role="alert">Divisi berhasil diperbaharui!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button></div>');
                    redirect('admin/show_division');
                }else{
                    $this->session->set_flashdata('error_division', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Terjadi kesalahan pada sistem!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button></div>');
                    redirect('admin/edit_division');
                }
            }
        }else{
            $data['division'] = $this->Team_Model->getDivision($id);
            $data['nav'] = 'team';
            $data['header'] = 'Edit';
            $this->load->view('admin/master/header', $data);
            $this->load->view('admin/team/add_division', $data);
            $this->load->view('admin/master/footer');
        }
	}
    public function delete_division($id)
    {
        if($this->session->id == null){
            redirect('auth');
            die();
        }

        $this->Team_Model->deleteDivision($id);
        $this->session->set_flashdata('error_division', '<div class="alert alert-success alert-dismissible fade show mx-2 mt-2" role="alert">Divisi berhasil dihapus!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
        redirect('admin/show_division');
    }
    /*
        *
        *
        GALERI
        *
        *
    */
    public function show_gallery()
    {
        if($this->session->id == null){
            redirect('auth');
            die();
        }

        $search = $this->input->get('search');
        if(isset($search)){
            $data['galleries'] = $this->Gallery_Model->searchGallery($search);
        }else{
            $data['galleries'] = $this->Gallery_Model->allGallery();
        }
        $data['nav'] = 'gallery';
        $this->load->view('admin/master/header', $data);
        $this->load->view('admin/gallery/index');
		$this->load->view('admin/master/footer');
    }
    public function add_gallery()
    {
        if($this->session->id == null){
            redirect('auth');
            die();
        }

        if($this->input->post()){
            $this->form_validation->set_rules('title', 'title', 'required|trim');

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error_gallery', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Semua kolom tidak boleh kosong!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button></div>');
                redirect('admin/add_gallery');
            }else{
                $title = $this->input->post('title');

                $upload = $this->upload_files('assets/img/upload/galleries/', ['picture']);
                if($upload){
                    date_default_timezone_set("Asia/Singapore");
                    $insert = [
                        'title' => htmlspecialchars($title),
                        'picture' =>  $upload[0],
                        'upload_at' => date('Y/m/d H:i:s'),
                    ];
    
                    if($this->Gallery_Model->addGallery($insert)){
                        $this->session->set_flashdata('error_gallery', '<div class="alert alert-success alert-dismissible fade show mx-2 mt-2" role="alert">Galeri berhasil ditambahkan!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button></div>');
                        redirect('admin/show_gallery');
                    }else{
                        $this->session->set_flashdata('error_gallery', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Terjadi kesalahan pada sistem!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button></div>');
                        redirect('admin/add_gallery');
                    }

                }else{
                    $this->session->set_flashdata('error_gallery', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Gagal saat upload gambar. Pastikan gambar berformat jpeg, jpg, atau png dan kurang dari 3Mb!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button></div>');
                    redirect('admin/add_gallery');
                }
            }
        }else{
            $data['nav'] = 'gallery';
            $data['header'] = 'Tambah';
            $this->load->view('admin/master/header', $data);
            $this->load->view('admin/gallery/add_gallery');
            $this->load->view('admin/master/footer');
        }
    }
    public function edit_gallery($id)
    {
        if($this->session->id == null){
            redirect('auth');
            die();
        }

        if($this->input->post()){
            $this->form_validation->set_rules('title', 'title', 'required|trim');

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error_gallery', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Semua kolom tidak boleh kosong!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button></div>');
                redirect('admin/edit_gallery');
            }else{
                $title = $this->input->post('title');

                $update = [
                    'title' => htmlspecialchars($title),
                ];

                if($this->Gallery_Model->updateGallery($id, $update)){
                    $this->session->set_flashdata('error_gallery', '<div class="alert alert-success alert-dismissible fade show mx-2 mt-2" role="alert">Galeri berhasil diperbaharui!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button></div>');
                    redirect('admin/show_gallery');
                }else{
                    $this->session->set_flashdata('error_gallery', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Terjadi kesalahan pada sistem!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button></div>');
                    redirect('admin/edit_gallery');
                }
            }
        }else{
            $data['nav'] = 'gallery';
            $data['header'] = 'Edit';
            $data['gallery'] = $this->Gallery_Model->getGallery($id);
            $this->load->view('admin/master/header', $data);
            $this->load->view('admin/gallery/add_gallery');
            $this->load->view('admin/master/footer');
        }
    }
    public function delete_gallery($id)
    {
        if($this->session->id == null){
            redirect('auth');
            die();
        }

        $gallery = $this->Gallery_Model->getGallery($id);
        $path = 'assets/img/upload/galleries/';
        if(unlink($path.$gallery->picture)){
            $this->Gallery_Model->deleteGallery($id);
            $this->session->set_flashdata('error_gallery', '<div class="alert alert-success alert-dismissible fade show mx-2 mt-2" role="alert">Galeri berhasil dihapus!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            redirect('admin/show_gallery');
        }else{
            $this->session->set_flashdata('error_gallery', '<div class="alert alert-danger alert-dismissible fade show mx-2 mt-2" role="alert">Terjadi kesalahan saat menghapus gambar!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            redirect('admin/show_gallery');
        }
        
    }
    /*
        *
        *
        UPLOAD
        *
        *
    */
    private function upload_files($path, $files)
    {
        $config = [
            'upload_path'   => $path,
            'allowed_types' => 'jpeg|jpg|gif|png',
            'overwrite'     => 1,
            'max_size'      => 3000                       
        ];

        $this->load->library('upload', $config);

        $images = [];

        foreach ($files as $i => $file) {

            $fileName = time().$i;;
            $config['file_name'] = $fileName;

            $this->upload->initialize($config);

            if ($this->upload->do_upload($file)) {
                $images[] = $this->upload->data()['file_name'];
            } else {
                return false;
            }
        }
        return $images;
    }
}
