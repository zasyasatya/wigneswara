<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	public function __construct() 
	{
		parent::__construct();
		$this->load->model('User_Model');
		$this->load->library('form_validation');
	}

	public function index()
	{
		if($this->session->userdata('id') != null){
			redirect('admin');
		}else{
			$this->load->view('auth/login');
		}
	}

	public function login() 
	{
		$this->form_validation->set_rules('username', 'Username', 'required|trim');
		$this->form_validation->set_rules('password', 'Password', 'required|trim');

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('error_login', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Username dan Password tidak boleh kosong! <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		  	</button></div>');
			redirect('auth');
		} else {
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$getUser = $this->User_Model->getUser($username);
			
			if ($getUser) {
				if(password_verify($password, $getUser->password)){
					$user= [
						'username' => $getUser->username,
						'id' => $getUser->id,
						'name' => $getUser->name
					];
					$this->session->set_userdata($user);
					redirect('Admin');
				}else{
					$this->session->set_flashdata('username', $username);
					$this->session->set_flashdata('error_login', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Password salah!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				  </button></div>');
					redirect('auth');
				}
			} else {
				$this->session->set_flashdata('error_login', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Username tidak terdaftar!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			  </button></div>');
				redirect('auth');
			}
		}
	}
	public function logout() 
	{
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('id');
		$this->session->unset_userdata('name');
		$this->session->set_flashdata('error_login', '<div class="alert alert-success" role="alert alert-dismissible fade show" >Anda berhasil keluar!<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">&times;</span>
	  </button></div>');
		redirect('auth');
	}

}