    <!-- Hero Section Start -->
    <div class="section position-relative overflow-hidden">

        <!-- Hero Slider Start -->
        <div class="">
            <div class="swiper-container">
                <div class="">

                    <!-- Hero Slider Item Start -->
                    <div class="hero-slide-item swiper-slide">

                        <div class="bg vw-100 overflow-hidden"></div>
                        <!-- Hero Slider Bg Image Start -->
                        <div class="hero-slide-bg vw-100" id="intro-video">
                            <video autoplay disablePictureInPicture muted loop>
                                <source src="<?= base_url('/assets/');?>/video/intro.m4v" type="video/mp4">
                            </video>
                        </div>
                        <div class="container vw-100 d-flex align-items-center text-center justify-content-center">
                            <div class="hero-slide-content text-center">
                                <h2 class="title">
                                    WIGNESWARA 
                                </h2>
                                <h1 class="text-white jargon">#SingMekeberSingMulih</h1>
                            </div>
                        </div>

                    </div>
                </div>

                <!-- Swiper Pagination Start -->
                <div class="swiper-pagination d-md-none"></div>
                <!-- Swiper Pagination End -->

            </div>
        </div>
        <!-- Hero Slider End -->

        <!-- Hero Slider Social Start -->
        <div class="hero-slider-social">

            <!-- Social Media Link Start -->
            <div class="social-media-link social-link-white">
                <a href="mailto:wigneswara.undiksha@gmail.com"><i class="fas fa-envelope"></i></a>
                <a href="https://www.instagram.com/wigneswara.undiksha/"><i class="fab fa-instagram"></i></a>
                <a href="https://www.youtube.com/channel/UCZfiR7n-ysynPCNW7qy8iig"><i class="fab fa-youtube"></i></a>
            </div>
            <!-- Social Media Link End -->

        </div>
        <!-- Hero Slider Social End -->

    </div>
    <!-- Hero Section End -->
    <div class="section section-padding-top overflow-hidden">
        <div class="container">
            <div class="row mb-n10 d-flex justify-content-between align-items-center">
                <div class="col-lg-6 mb-10 col-md-12 order-2 order-lg-1" data-aos="fade-right" data-aos-delay="500">
                    <div class="history-wrapper">
                        <h1 class="title">Tim Wigneswara Undiksha</h1>
                        <div class="history-content">
                            <h4 class="subtitle">Nama Wigneswara terinspirasi dari gelar dari Dewa Ganesha yaitu Wigneswara yang artinya dewa segala rintangan.</h4>
                            <p>Atas dasar tersebut Tim Wigneswara menggunakan nama tersebut yang menggambarkan kemampuan Tim Wigneswara dalam menghadapi berbagai rintangan. Sehingga Tim Wigneswara dapat menjadi tim yang tangguh. Tim wigneswara terdiri dari 3 orang tim inti, sekretaris, bendahara, serta tim pendukung yang dibagi menjadi 5 divisi yaitu administrasi, multimedia, web dev, manufaktur dan sarana prasarana. Saat ini Tim Wigneswara diketuai oleh Putu Zasya Eka Satya Nugraha dan dibimbing oleh Dr. I Made Gede Sunarya, S.Kom., M.Cs.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 mb-10 col-md-12 align-self-center order-1 order-lg-2" data-aos="fade-left" data-aos-delay="500">
                    <div class="history-image d-flex justify-content-center">
                        <img class="fit-image inti-img" src="<?= base_url('/assets/');?>img/static/inti.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section section-padding-top section-padding-bottom overflow-hidden">
        <div class="container">
            <div class="row mb-n10 flex-column-reverse flex-lg-row flex-md-row">
                <div class="col-lg-6 mb-10 col-md-12 order-2 order-lg-1" data-aos="fade-right" data-aos-delay="500">
                    <div class="history-image">
                        <img class="fit-image plane-img" src="<?= base_url('/assets/');?>img/static/wahana.png" alt="">
                    </div>
                </div>
                <div class="col-lg-6 mb-10 col-md-12 align-self-center order-1 order-lg-2" data-aos="fade-left" data-aos-delay="500">
                    <div class="history-wrapper">
                        <h1 class="title">Detail Pesawat Wigneswara</h1>
                        <div class="history-content">
                            <h4 class="subtitle">Ekadanta-821 mengadaptasi desain wahana racing dengan tipe low-wings yang memungkinkan manuver yang lebih baik.</h4>
                            <p>Wahana bertipe pusher dengan motor brushless 2184 1100KV yang kecepatannya dikontrol dengan ESC 60A dan ditenagai oleh Lipo 3300 mAh 4s. Ekadanta-821 menggunakan sistem otomasi Pixhawk PX4 dengan telemetry receiver 16Ch. Dimensi propeller yang digunakan pada Ekadanta-821 yaitu 9x6 material plastik. Karakteristik dari wahana ini yaitu kombinasi stabilitas dan kelincahan yang baik.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Project Tab Section Start -->
    <div class="section  bg-light">
        <div class="container">
            <div class="row" data-aos="fade-up" data-aos-delay="300">
                <!-- Section Title Start -->
                <div class="col-xl-3 col-md-12">
                    <div class="section-title mb-5">
                        <h2 class="title text-center mb-5" style="text-align: left !important;">Galeri Kegiatan</h2>
                    </div>
                </div>
                <!-- Section Title End -->

                <!-- Tab Start -->
                <div class="col-xl-7 col-md-8 col-sm-12">

                    <!-- Section Title & Product Tab Start -->
                    <div class="section-tabs-header">

                    </div>
                    <!-- Section Title & Product Tab End -->
                </div>
                <!-- Tab End -->

            </div>
        </div>
        <div class="container-auto">
            <!-- Tab Content Start -->
            <div class="tab-content" data-aos="fade-up" data-aos-delay="400">
                <div class="tab-pane fade show active" id="tab-item-all">
                    <div class="tab-pane-carousel position-relative">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <?php foreach($galleries as $i=>$gallery): ?>
                                <div class="swiper-slide">
                                    <!-- Single Project Slide Start -->
                                    <div class="single-project-slide">

                                        <!-- Thumb Start -->
                                        <div class="thumb">
                                            <div class="image">
                                                <img class="fit-image gambar" src="<?= base_url('/assets/img/upload/galleries/'. $gallery->picture);?>" alt="Product" />
                                            </div>
                                        </div>
                                        <!-- Thumb End -->

                                        <!-- Content Start -->
                                        <div class="content">
                                            <h4 class="subtitle">Galeri</h4>
                                            <h3 class="title"><a href="progress.html"><?=$gallery->title?></a></h3>
                                        </div>
                                        <!-- Content End -->

                                    </div>
                                    <!-- Single Project Slide End -->
                                </div>
                                <?php endforeach; ?>
                            </div>

                            <!-- Swiper Pagination Start -->
                            <div class="swiper-pagination d-none"></div>
                            <!-- Swiper Pagination End -->

                            <!-- Swiper Navigation Start -->
                            <div class="tab-carousel-prev swiper-button-prev"><i class="icofont-thin-left"></i></div>
                            <div class="tab-carousel-next swiper-button-next"><i class="icofont-thin-right"></i></div>
                            <!-- Swiper Navigation End -->
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="tab-item-architecture">
                    <div class="tab-pane-carousel-two position-relative">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">

                                <div class="swiper-slide">
                                    <!-- Single Project Slide Start -->
                                    <div class="single-project-slide">

                                        <!-- Thumb Start -->
                                        <div class="thumb">
                                            <a href="progress.html" class="image">
                                                <img class="fit-image" src="assets/images/gallery/abu-abu.jpg" alt="Product" />
                                            </a>
                                        </div>
                                        <!-- Thumb End -->

                                        <!-- Content Start -->
                                        <div class="content">
                                            <h4 class="subtitle">Residential</h4>
                                            <h3 class="title"><a href="progress.html">Cubic Villa</a></h3>
                                        </div>
                                        <!-- Content End -->

                                    </div>
                                    <!-- Single Project Slide End -->
                                </div>

                                <div class="swiper-slide">
                                    <!-- Single Project Slide Start -->
                                    <div class="single-project-slide">

                                        <!-- Thumb Start -->
                                        <div class="thumb">
                                            <a href="progress.html" class="image">
                                                <img class="fit-image" src="assets/images/gallery/abu-abu.jpg" alt="Product" />
                                            </a>
                                        </div>
                                        <!-- Thumb End -->

                                        <!-- Content Start -->
                                        <div class="content">
                                            <h4 class="subtitle">Architecture</h4>
                                            <h3 class="title"><a href="progress.html">Culture House</a></h3>
                                        </div>
                                        <!-- Content End -->

                                    </div>
                                    <!-- Single Project Slide End -->
                                </div>

                                <div class="swiper-slide">
                                    <!-- Single Project Slide Start -->
                                    <div class="single-project-slide">

                                        <!-- Thumb Start -->
                                        <div class="thumb">
                                            <a href="progress.html" class="image">
                                                <img class="fit-image" src="assets/images/gallery/abu-abu.jpg" alt="Product" />
                                            </a>
                                        </div>
                                        <!-- Thumb End -->

                                        <!-- Content Start -->
                                        <div class="content">
                                            <h4 class="subtitle">Commercial</h4>
                                            <h3 class="title"><a href="progress.html">ABC Financial Bank</a></h3>
                                        </div>
                                        <!-- Content End -->

                                    </div>
                                    <!-- Single Project Slide End -->
                                </div>

                                <div class="swiper-slide">
                                    <!-- Single Project Slide Start -->
                                    <div class="single-project-slide">

                                        <!-- Thumb Start -->
                                        <div class="thumb">
                                            <a href="progress.html" class="image">
                                                <img class="fit-image" src="assets/images/gallery/abu-abu.jpg" alt="Product" />
                                            </a>
                                        </div>
                                        <!-- Thumb End -->

                                        <!-- Content Start -->
                                        <div class="content">
                                            <h4 class="subtitle">Interior</h4>
                                            <h3 class="title"><a href="progress.html">B6-No.5 OLA Tower</a></h3>
                                        </div>
                                        <!-- Content End -->

                                    </div>
                                    <!-- Single Project Slide End -->
                                </div>

                            </div>

                            <!-- Swiper Pagination Start -->
                            <div class="swiper-pagination d-none"></div>
                            <!-- Swiper Pagination End -->

                            <!-- Swiper Navigation Start -->
                            <div class="tab-carousel-prev swiper-button-prev"><i class="icofont-thin-left"></i></div>
                            <div class="tab-carousel-next swiper-button-next"><i class="icofont-thin-right"></i></div>
                            <!-- Swiper Navigation End -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- Tab Content End -->
        </div>
    </div>
    <!-- Project Tab Section End -->
