    <!-- Breadcrumb Section Start -->
    <div class="section section-padding">
        <div class="container">
            <div class="row">
                <div class="col-12">

                    <!-- breadcrumb Wrapper Start -->
                    <div class="breadcrumb-wrapper">
                        <!-- Bread Title Start -->
                        <div class="bread-title">
                            <h1 class="title">Kegiatan</h1>
                        </div>
                        <!-- Bread Title End -->
                    </div>
                    <!-- breadcrumb Wrapper End -->

                </div>
            </div>

        </div>
    </div>
    <!-- Breadcrumb Section End -->

    <!-- Blog Section Start Here -->
    <div class="section blog-tab-section">
        <div class="container">
            <div class="row mt-n2">

                <!-- blog Menu Start -->
                <div class="messonry-button text-center mb-10" data-aos="fade-up" data-aos-delay="100">
                    <button data-filter="*" class="is-checked port-filter">Semua</button>
                    <?php foreach($categories as $i=>$category): ?>
                    <button data-filter=".cat-<?=$category->id?>" class="port-filter"><?=$category->name?></button>
                    <?php endforeach; ?>
                </div>
                <!-- blog Menu End -->
                        
            </div>

            <div class="row row-cols-1 mesonry-list mb-n10">

                <div class="resizer col"></div>
                <?php foreach($activities as $i=>$activity): ?>       
                <!-- Single blog Start -->
                <div class="col cat-<?=$activity->category_id?> mb-10">
                    <div class="single-blog-wrap">
                        <div class="blog-thumb">
                            <a class="image" href="<?=base_url('activity/'.$activity->id)?>">
                                <img class="fit-image" src="<?=base_url('/assets/img/upload/activities/'.$activity->picture1)?>" alt="blog Image">
                            </a>
                        </div>
                        <div class="inner-content">
                            <ul class="info-list">
                                <li><?=$activity->date?></li>
                                <li><?=$activity->category?></li>
                            </ul>
                            <h4 class="title"><a href="<?=base_url('activity/'.$activity->id)?>"><?=$activity->title?></a></h4>
                            <p><?=$activity->description?></p>
                            <a href="<?=base_url('activity/'.$activity->id)?>" class="article">Lihat selengkapnya <span class="arrow icofont-rounded-right"></span></a>
                        </div>
                    </div>
                </div>
                <!-- Single blog End -->
                <?php endforeach; ?>
            </div>
            <div class="row section-padding">
                <div class="col-12">
                <?php if(count($activities) == 0): ?>
                    <div class="load-more text-center" data-aos="fade-up" data-aos-delay="300">
                        <p href="#">Belum ada kegiatan.<p/>
                    </div>
                <?php endif; ?> 
                </div>
            </div>

        </div>
    </div>
    <!-- Blog Section End Here -->
