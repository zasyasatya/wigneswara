 <!-- Breadcrumb Section Start -->
 <div class="section section-padding">
        <div class="container">
            <div class="row">
                <div class="col-12">

                    <!-- breadcrumb Wrapper Start -->
                    <div class="breadcrumb-wrapper">
                        <!-- Bread Title Start -->
                        <div class="bread-title">
                            <h1 class="title">Tim Wigneswara</h1>
                        </div>
                        <!-- Bread Title End -->
                    </div>
                    <!-- breadcrumb Wrapper End -->

                </div>
            </div>

        </div>
    </div>

    <div class="container">
    <div class="section team">
        <div class="form-group">
            <!-- <label for="exampleFormControlInput1"></label> -->
            <div class="row">
                <div class="col-md-10">
                    <input type="email" class="form-control" id="searchTeam" placeholder="Cari anggota tim...">
                </div>
                <div class="col-md-2">
                    <button class="btn btn-primary form-control" onclick="searchTeam('#searchTeam', '#tableTeam')" type="submit">Cari!</button>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped mt-5 mb-9" id="tableTeam">
                <thead>
                    <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Prodi</th>
                    <th scope="col">Divisi</th>
                    <th scope="col">Jabatan</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($teams as $i=>$team): ?>
                    <tr>
                    <th scope="row"><?=$i + 1?></th>
                    <td><?=$team->name?></td>
                    <td><?=$team->prodi?></td>
                    <td><?=$team->division?></td>
                    <td><?=$team->position?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        
        </div>
    </div>
    </div>
    