<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link
      href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;700&display=swap"
      rel="stylesheet"
    />
    <link rel="stylesheet" href="<?= base_url('/assets/');?>css/404-style.css" />
    <title>Not Found</title>
  </head>
  <body>
    <div class="container">
      <div class="img">
        <img src="<?= base_url('/assets/');?>img/system/404.svg" alt="" />
      </div>
      <div class="text">
        <p>Pesawat anda mengalami kerusakan yang fatal. Kembali ke bengkel sekarang!</p>
        <a href="<?= $this->session->userdata('referred_from')?>">Kembali</a>
      </div>
    </div>
  </body>
</html>
