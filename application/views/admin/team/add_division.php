<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

<!-- Main Content -->
<div id="content">

  <!-- Begin Page Content -->
  <div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800 mt-5 mb-3"><?=$header?> Divisi</h1>
    <!-- DataTales Example -->
    <div class="card shadow mb-4 p-4 col-12 col-lg-8">
      <form method="POST" action="">
      <?=$this->session->flashdata('error_division');?>
          <div class="form-group">
            <label for="title">Nama Divisi</label>
            <input type="text" class="form-control" id="title" placeholder="Masukan nama divisi" name="name" required autocomplate="off" value="<?=isset($division) ? $division->name :''?>">
          </div>
          <div class="form-group">
              <label for="description">Deskripsi</label>
              <textarea class="form-control" id="description" rows="3" name="description" required placeholder="Masukan deskripsi divisi"><?=isset($division) ? $division->description :''?></textarea>
          </div>
          <button type="submit" class="btn btn-primary mt-4 float-right">Simpan</button>
        </form>
    </div>

  </div>
  <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
