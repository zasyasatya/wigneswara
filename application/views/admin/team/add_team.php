<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

<!-- Main Content -->
<div id="content">

  <!-- Begin Page Content -->
  <div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800 mt-5 mb-3"><?=$header?> Anggota</h1>
    <!-- DataTales Example -->
    <div class="card shadow mb-4 p-4 col-12 col-lg-8">
      <?=$this->session->flashdata('error_team');?>
      <form method="POST" action="">
          <div class="form-group">
            <label for="nim">NIM</label>
            <input type="number" class="form-control" required id="nim" placeholder="Masukan NIM anggota" autocomplete = "off" name="nim" value="<?=isset($team) ? $team->nim :''?>">
          </div>
          <div class="form-group">
            <label for="name">Nama</label>
            <input type="text" class="form-control" required id="name" placeholder="Masukan nama anggota" autocomplete = "off" name="name" value="<?=isset($team) ? $team->name :''?>">
          </div>
          <div class="form-group">
            <label for="prodi">Program Studi</label>
            <input type="text" class="form-control" required id="prodi" placeholder="Masukan prodi anggota" autocomplete = "off" name="prodi" value="<?=isset($team) ? $team->prodi :''?>">
          </div>
          <div class="form-group">
            <label for="divition" >Devisi</label>
            <select class="form-control" required id="division" name="division">
              <?php foreach($divisions as $i=>$division): ?>
                <option value="<?=$division->id?>"<?= isset($team) ? $team->division == $division->id ? "selected":'':''?>><?=$division->name?></option>
              <?php endforeach; ?>
            </select>
          </div>
          <div class="form-group">
            <label for="positon" >Jabatan</label>
            <select class="form-control" required id="position" name="position">
              <?php foreach($positions as $i=>$position): ?>
                <option value="<?=$position->id?>"<?= isset($team) ? $team->position == $position->id ? "selected":'':''?>><?=$position->name?></option>
              <?php endforeach; ?>
            </select>
          </div>
          <button type="submit" class="btn btn-primary mt-4 float-right">Simpan</button>
        </form>
    </div>

  </div>
  <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
