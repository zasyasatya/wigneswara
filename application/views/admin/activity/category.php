    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800 mt-5 mb-3">Kategori</h1>
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3 d-flex justify-content-between">
              <h6 class="m-0 font-weight-bold text-primary">Data kategori</h6>
              <a href="<?=base_url('admin/add_category')?>" class="btn btn-primary">Tambah</a>
            </div>
            <?=$this->session->flashdata('error_category');?>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Kategori</th>
                      <th>Deskripsi</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Kategori</th>
                        <th>Deskripsi</th>
                        <th>Aksi</th>
                    </tr>
                  </tfoot>
                  <tbody>
                  <?php foreach($categories as $i=>$category): ?>
                    <tr>
                      <td><?=$i + 1?></td>
                      <td><?=$category->name?></td>
                      <td><?=$category->description?></td>
                      <td>
                        <div class="d-flex flex-row" data-toggle="buttons">
                            <a href="<?=base_url('admin/edit_category/'.$category->id)?>" class="btn btn-warning mr-2">Edit</a>
                            <button type="button" onclick="lauchModal('#deleteCategory', '<?= $category->name; ?>', '<?=base_url('admin/delete_category/'.$category->id)?>')" data-toggle="modal" data-target="#deleteCategory" class="btn btn-danger">Hapus</button>
                          </div>
                      </td>
                    </tr>
                  <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
      <div class="modal fade" id="deleteCategory" tabindex="-1" role="dialog" aria-labelledby="deleteCategoryTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Hapus Devisi</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            Apakah anda yakin ingin menghapus devisi <span class="font-weight-bold" id="titleModal"></span>?
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <a id="linkModal" href="" class="btn btn-danger">Hapus</a>
          </div>
        </div>
      </div>
    </div>