<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

<!-- Main Content -->
<div id="content">

  <!-- Begin Page Content -->
  <div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800 mt-5 mb-3"><?=$header?> Kategori</h1>
    <!-- DataTales Example -->
    <div class="card shadow mb-4 p-4 col-12 col-lg-8">
      <?=$this->session->flashdata('error_category');?>
      <form method="POST" action="">
          <div class="form-group">
            <label for="title">Nama Kategori</label>
            <input type="text" class="form-control" id="title" placeholder="Masukan nama kategori" autocomplete="off" required name="name" value="<?=isset($category) ? $category->name :''?>">
          </div>
          <div class="form-group">
              <label for="description">Deskripsi</label>
              <textarea class="form-control" id="description" rows="3" placeholder="Masukan deskripsi kategori" required name="description"><?=isset($category) ? $category->description :''?></textarea>
          </div>
          <button type="submit" class="btn btn-primary mt-4 float-right">Simpan</button>
        </form>
    </div>

  </div>
  <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

