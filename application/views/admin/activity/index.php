 <!-- Content Wrapper -->
 <div id="content-wrapper" class="d-flex flex-column">

<!-- Main Content -->
<div id="content">
  <!-- Begin Page Content -->
  <div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4 mt-5">
      <h1 class="h3 mb-0 text-gray-800">Daftar Kegiatan</h1>
    </div>

    <!-- search Row -->
    <div class="row col-lg-9 col-12 d-flex justify-content-between align-items-start">
      <form action="" method="GET" class="input-group mb-3 col-lg-10 col-12">
          <input type="text" class="form-control" autocomplete="off" placeholder="Cari kegiatan..." aria-label="Cari kegiatan" aria-describedby="basic-addon2"  name="search">
          <div class="input-group-append">
            <button class="btn btn-primary" type="submit"><i class="fas fa-search"></i></button>
          </div>
      </form>
      <a href="<?=base_url('admin/add_activity')?>" class="btn btn-primary ml-2 mb-3">Tambah</a>
  </div>

  <!-- content row -->
  <?=$this->session->flashdata('error_activity');?>
  <div class="row mx-1 mb-2">
      <?php foreach($activities as $i=>$activity): ?>
        <div class="card shadow col-12 col-md-12 col-lg-3 my-2 mx-2 align-items-start">
            <img class="card-img-top mt-3" src="<?=base_url('assets/img/upload/activities/'.$activity->picture1)?>" alt="Gambar 1">
            <div class="card-body">
              <h5 class="card-title"><?=$activity->title?></h5>
              <p class="card-text"><?=$activity->category?></p>
            </div>
            <div class="card-body">
              <a href="" class="card-link text-danger" onclick="lauchModal('#deleteActivity', '<?= $activity->title; ?>', '<?=base_url('admin/delete_activity/'.$activity->id)?>')" data-toggle="modal" data-target="#deleteActivity">Hapus</a>
              <a href="<?=base_url('admin/edit_activity/'.$activity->id)?>" class="card-link">Edit</a>
              <a target="_blank" href="<?=base_url('activity/'.$activity->id)?>" class="card-link">Lihat</a>
            </div>
        </div>
      <?php endforeach; ?> 
      <?php if(count($activities) == 0): ?>
        <p class="mt-4">Tidak ada data kegiatan</p>
      <?php endif; ?> 
  </div>

  </div>
  <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
<div class="modal fade" id="deleteActivity" tabindex="-1" role="dialog" aria-labelledby="deleteActivityTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Hapus Devisi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah anda yakin ingin menghapus kegiatan <span class="font-weight-bold" id="titleModal"></span>?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <a id="linkModal" href="" class="btn btn-danger">Hapus</a>
      </div>
    </div>
  </div>
</div>
