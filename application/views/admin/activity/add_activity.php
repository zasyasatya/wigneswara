<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

<!-- Main Content -->
<div id="content">

  <!-- Begin Page Content -->
  <div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800 mt-5 mb-3"><?=$header?> Kegiatan</h1>
    <!-- DataTales Example -->
    <div class="card shadow mb-4 p-4 col-12 col-lg-8">
      <?=$this->session->flashdata('error_activity');?>
      <form method="POST" action="" enctype="multipart/form-data">
          <div class="form-group">
            <label for="title">Judul</label>
            <input type="text" class="form-control" required autocomplete="off" id="title" placeholder="Masukan judul kegiatan" name="title" value="<?=isset($activity) ? $activity->title :''?>">
          </div>
          <div class="form-group">
            <label for="category" >Kategori</label>
            <select class="form-control" required id="category" name="category">
              <?php foreach($categories as $i=>$category): ?>
                <option value="<?=$category->id?>"<?= isset($activity) ? $activity->category == $category->name ? "selected":'':''?>><?=$category->name?></option>
              <?php endforeach; ?>
            </select>
          </div>
          <div class="form-group">
              <label for="content_text" >Konten</label>
              <textarea class="form-control" required id="content_text" rows="3" placeholder="Masukan deskripsi kegiatan" name="content""><?=isset($activity) ? $activity->content :''?></textarea>
          </div>
          <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text">Gambar 1</span>
              </div>
              <div class="custom-file">
                <input type="file" required  accept="image/*" class="custom-file-input" id="pic1" name="picture1" onchange="fileInputHandler(event)" <?=isset($activity) ? 'disabled' :''?>>
                <label class="custom-file-label" for="pic1"><?=isset($activity) ? $activity->picture1 :'Pilih gambar'?></label>
              </div>
          </div>
          <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text">Gambar 2</span>
              </div>
              <div class="custom-file">
                <input type="file" required  accept="image/*" class="custom-file-input" id="pic2" name="picture2" onchange="fileInputHandler(event)" <?=isset($activity) ? 'disabled' :''?>>
                <label class="custom-file-label" for="pic2"><?=isset($activity) ? $activity->picture2 :'Pilih gambar'?></label>
              </div>
          </div>
          <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">URL</span>
              </div>
              <input type="text" class="form-control" required autocomplete="off" placeholder="Link video Youtube" aria-label="yt" aria-describedby="basic-addon1" name="video" value="<?=isset($activity) ? $activity->video :''?>" <?=isset($activity) ? 'disabled' :''?>>
            </div>
            <script>
                CKEDITOR.replace( 'content_text');
            </script>
          <button type="submit" class="btn btn-primary mt-4 float-right">Simpan</button>
        </form>
    </div>

  </div>
  <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
