<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

<!-- Main Content -->
<div id="content">

  <!-- Begin Page Content -->
  <div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800 mt-5 mb-3"><?=$header?> Galeri</h1>
    <!-- DataTales Example -->
    <div class="card shadow mb-4 p-4 col-12 col-lg-8">
      <?=$this->session->flashdata('error_gallery');?>
      <form method="POST" action="" enctype="multipart/form-data">
          <div class="form-group">
            <label for="title">Judul</label>
            <input type="text" class="form-control" id="title" autocomplete="off" name="title" required placeholder="Masukan judul" value="<?=isset($gallery) ? $gallery->title:''?>"">
          </div>
          <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text">Gambar</span>
              </div>
              <div class="custom-file">
                <input type="file" class="custom-file-input" accept="image/*"  id="pic1" name="picture" required onchange="fileInputHandler(event)" <?=isset($gallery) ? 'disabled':''?>>
                <label class="custom-file-label" for="pic1"><?=isset($gallery) ? $gallery->picture:'Pilih gambar'?></label>
              </div>
          </div>
          <button type="submit" class="btn btn-primary mt-4 float-right">Simpan</button>
        </form>
    </div>

  </div>
  <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
