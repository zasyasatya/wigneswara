<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

<!-- Main Content -->
<div id="content">

  <!-- Begin Page Content -->
  <div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800 mt-5 mb-3">Tentang</h1>
    <!-- DataTales Example -->
    <div class="card shadow mb-4 p-4 col-12 col-lg-8">
        <div class="mb-2">
            <h5 class="tex-primary">Versi Sistem</h5>
            <p>V.1.1.0</p>
        </div>
        <div class="mb-2">
            <h5 class="tex-primary">Deskripsi Sistem</h5>
            <p>Sistem berjalan pada PHP 5.6 menggunakan framework Code Igniter 3 dengan Database menggunakan MySQL. Sistem bertujuan untuk menampilkan deskripsi tentang Wigneswara serta kegiatan yang dilakukan oleh anggota.</p>
        </div>
        <div class="mb-2">
          <h5 class="tex-primary">Dibuat Oleh</h5>
          <p>Divisi Pengembang Web Wigneswara</p>
        </div>
        <div class="mb-2">
          <h5 class="tex-primary">Tahun Dibuat</h5>
          <p>Mei 2021</p>
        </div>
        <div class="mb-2">
          <h5 class="tex-primary">Terakhir Update</h5>
          <p>Mei 2021</p>
        </div>
    </div>

  </div>
  <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
