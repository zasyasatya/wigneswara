<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

<!-- Main Content -->
<div id="content">

  <!-- Begin Page Content -->
  <div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800 mt-5 mb-3">Pengaturan</h1>
    <!-- DataTales Example -->
    <div class="card shadow mb-4 p-4 col-12 col-lg-8">
      <?=$this->session->flashdata('error_update');?>
      <form method = "POST" action="<?=base_url()?>admin/update_user">
          <input type="hidden" id="id" name="id" value="<?=$this->session->id?>">
          <div class="form-group">
            <label for="name">Nama</label>
            <input type="text" class="form-control" id="name" required placeholder="Masukan nama" name="name" value="<?=$this->session->name?>">
          </div>
          <div class="form-group">
              <label for="username">Username</label>
              <input type="text" class="form-control" id="username" required placeholder="Masukan username" name="username" value="<?=$this->session->username?>">
          </div>
          <div class="form-group">
              <label for="password">Password</label>
              <input type="password" class="form-control" id="password" name="password" placeholder="Masukan password">
          </div>
          <div class="form-group">
              <label for="confirm">Konfirmasi Password</label>
              <input type="password" class="form-control" id="confirm" name="confirm" placeholder="Masukan ulang password">
          </div>
          <p>Kosongkan password jika tidak ingin mengganti</p>
          <button type="submit" class="btn btn-primary mt-4 float-right">Simpan</button>
        </form>
    </div>

  </div>
  <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

