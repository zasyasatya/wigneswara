<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Wigneswara Admin</title>
  <link rel="shortcut icon" href="<?= base_url('/assets');?>/img/wg-logo.png" type="image/x-icon" />

  <!-- Custom fonts for this template-->
  <link href="<?= base_url('/assets/');?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?= base_url('/assets/');?>css/sb-admin-2.min.css" rel="stylesheet">
  <link href="<?= base_url('/assets/');?>vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
  <script src="<?= base_url('/assets/');?>vendor/ckeditor/ckeditor.js"></script>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-text mx-3"><?=$this->session->name?></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item <?= $nav == 'home'? 'active' : '';?>">
        <a class="nav-link" href="<?= base_url();?>admin">
          <i class="fas fa-home"></i>
          <span>Beranda</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Fitur
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item <?= $nav == 'activity'? 'active' : '';?>">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-calendar-plus"></i>
          <span>Kegiatan</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Kelola Kegiatan</h6>
            <a class="collapse-item" href="<?= base_url();?>admin/show_activity">Daftar Kegiatan</a>
            <a class="collapse-item" href="<?= base_url();?>admin/show_category">Kategori</a>
          </div>
        </div>
      </li>

      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item <?= $nav == 'team'? 'active' : '';?>">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-users"></i>
          <span>Tim</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Kelola Tim</h6>
            <a class="collapse-item" href="<?= base_url();?>admin/show_team">Anggota Tim</a>
            <a class="collapse-item" href="<?= base_url();?>admin/show_division">Divisi</a>
          </div>
        </div>
      </li>

      <!-- Nav Galeri -->
      <li class="nav-item <?= $nav == 'gallery'? 'active' : '';?>">
        <a class="nav-link" href="<?= base_url();?>admin/show_gallery">
          <i class="fas fa-images"></i>
          <span>Galeri</span></a>
      </li>
      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Lainnya
      </div>

      <!-- Nav Pengaturan -->
      <li class="nav-item <?= $nav == 'setting'? 'active' : '';?>">
        <a class="nav-link" href="<?= base_url();?>admin/show_setting">
          <i class="fas fa-fw fa-cog"></i>
          <span>Pengaturan</span></a>
      </li>

      <!-- Nav Item -->
      <li class="nav-item <?= $nav == 'about'? 'active' : '';?>">
        <a class="nav-link" href="<?= base_url();?>admin/show_about">
          <i class="fas fa-info-circle"></i>
          <span>Tentang</span></a>
      </li>

      <!-- Nav Item -->
      <li class="nav-item">
        <a class="nav-link " href="charts.html" href="#" data-toggle="modal" data-target="#logoutModal">
          <i class="fas fa-sign-out-alt"></i>
          <span>Keluar</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->