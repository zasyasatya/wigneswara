const lauchModal = (id, title, link) => {
    const titleModal = document.querySelector(id+' #titleModal'),
          linkModal = document.querySelector(id+' #linkModal');
    titleModal.innerHTML = title;
    linkModal.href = link;
}