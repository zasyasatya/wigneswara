const searchTeam = (inputId, tableId) => {
    const search = document.querySelector(inputId),
          table = document.querySelector(tableId),
          tr = table.getElementsByTagName("tr");

    let txtValue, keyword = search.value.toUpperCase();

    
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
        txtValue = td.textContent || td.innerText;
        if (txtValue.toUpperCase().indexOf(keyword) > -1) {
            tr[i].style.display = "";
        } else {
            tr[i].style.display = "none";
        }
        }       
    }

    search.value = "";
}